## hadoop word count
The scenario is counting the number of smaller number of occurances of two files. For example, John occurs 5 times in file1 and 3 times in file2, 3 times is number of occurances for John. Stop words need to be removed before counting the occurances. Top 20 number of occurances need to be found in the two files.

## Pack .jar file
1 export hadoop_home
```
export HADOOP_HOME=/usr/local/Cellar/hadoop/3.3.4/libexec 
```

2 build class file
```
 javac -source 1.8 -target 1.8 -bootclasspath /Library/Java/JavaVirtualMachines/jdk1.8.0_171.jdk/Contents/Home/jre/lib/rt.jar -classpath $HADOOP_HOME/share/hadoop/common/hadoop-common-3.3.4.jar:/usr/local/Cellar/hadoop/3.3.4/libexec/share/hadoop/mapreduce/hadoop-mapreduce-client-core-3.3.4.jar:$HADOOP_HOME/share/hadoop/common/lib/commons-cli-1.2.jar FollowersCount.java 
```

3 move class file to output
```
mv src/main/java/*.class target/classes/
```

4 create jar file
```
jar cvfm FollowersCount.jar ../../manifest.txt -C target/classes . 
```

5 run jar file
```
/Library/Java/JavaVirtualMachines/jdk1.8.0_171.jdk/Contents/Home/bin/java -cp "FollowersCount.jar:$HADOOP_HOME/share/hadoop/common/*:$HADOOP_HOME/share/hadoop/mapreduce/*:$HADOOP_HOME/share/hadoop/hdfs/*:$HADOOP_HOME/share/hadoop/common/lib/*:$HADOOP_HOME/share/hadoop/yarn/*" FollowersCount
```