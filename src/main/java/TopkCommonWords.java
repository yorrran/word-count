import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class TopkCommonWords {
    public static Integer Top_K = 20;

    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, MapWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        private HashSet<String> wordsToSkip = new HashSet<String>();
        private String stopWordFileName;

        protected void setup(Mapper.Context context) {
            Configuration conf = context.getConfiguration();
            File stopWordsPath = new File(conf.get("stopwords.path"));
            stopWordFileName = stopWordsPath.getName();
            if (stopWordsPath.toString() != null) {
                try {
                    BufferedReader fis = new BufferedReader(new FileReader(stopWordsPath));
                    String stopWord;
                    while ((stopWord = fis.readLine()) != null) {
                        wordsToSkip.add(stopWord);
                    }
                } catch (IOException ioe) {
                    System.err.println("Stop work exception" + "' : " + StringUtils.stringifyException(ioe));
                }
            }

        }

        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {
            String currFileName = ((FileSplit) context.getInputSplit()).getPath().getName();
            if (!currFileName.equals(stopWordFileName)) {
                StringTokenizer itr = new StringTokenizer(value.toString());
                while (itr.hasMoreTokens()) {
                    String token = itr.nextToken();
                    if (token != null & !wordsToSkip.contains(token)) {
                        word.set(token);
                        MapWritable wordHashMap = new MapWritable();
                        wordHashMap.put(word, one);
                        Text currFile = new Text();
                        currFile.set(currFileName);
                        context.write(currFile, wordHashMap);
                    }
                }
            }
        }
    }


    public static class IntSumReducer
            extends Reducer<Text, MapWritable, Text, MapWritable> {
        private IntWritable result = new IntWritable();
        private HashMap<Writable, Integer> currHashMap;

        public void reduce(Text key, Iterable<MapWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
            currHashMap = new HashMap<Writable, Integer>();
            for (MapWritable val : values) {
                for (Writable writableKey : val.keySet()) {
                    if (currHashMap.containsKey(writableKey)) {
                        int value = currHashMap.get(writableKey);
                        value = value + ((IntWritable) val.get(writableKey)).get();
                        currHashMap.put(writableKey, value);
                    } else {
                        currHashMap.put(writableKey, ((IntWritable) val.get(writableKey)).get());

                    }
                }
            }

            for (Writable countKey : currHashMap.keySet()) {
                MapWritable writableMap = new MapWritable();
                IntWritable sum = new IntWritable(currHashMap.get(countKey));
                writableMap.put(countKey, sum);
                context.write(key, writableMap);
            }
        }
    }

    public static class TopkMapper
            extends Mapper<Writable, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);

        public void map(Writable key, Text value, Context context
        ) throws IOException, InterruptedException {
            String[] str = value.toString().split("\\s+");
            String wordMap = str[1].replaceAll("[{}]", " ").trim();
            String[] keyValue = wordMap.split("=");
            Text text = new Text();
            text.set(keyValue[0]);
            IntWritable one = new IntWritable(Integer.parseInt(keyValue[1]));
            context.write(text, one);
        }
    }

    public static class TopkReducer
            extends Reducer<Text, IntWritable, IntWritable, Text> {
        private HashMap<String, Integer> wordValueHashMap = new HashMap<>();

        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context
        ) throws IOException, InterruptedException {
//            System.out.println("topkReducer:" + key + values);
            List<Integer> list = new ArrayList<>();
            for (IntWritable value : values) {
                list.add(value.get());
            }
            Integer minValue = Collections.min(list, null);
            if (list.size() >= 2) {
                wordValueHashMap.put(key.toString(), minValue);
            }
        }

        public void cleanup(Context context) throws IOException, InterruptedException {
            wordValueHashMap =
                    wordValueHashMap.entrySet().stream()
                            .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                    (e1, e2) -> e2, LinkedHashMap::new));

            List<String> topKWordHashMap = wordValueHashMap.entrySet().stream().map(Map.Entry::getKey).limit(Top_K).collect(Collectors.toList());
            for (String key : topKWordHashMap) {
                Text text = new Text();
                text.set(key);
                IntWritable one = new IntWritable(wordValueHashMap.get(key));
                context.write(one, text);
            }
        }
    }


    public static void main(String[] args) throws Exception {
        String workingDir = System.getProperty("user.dir");
        Path inputPath1 = args.length >= 1 ? new Path(args[0]) : new Path("/Users/pangyaran/Documents/master-semester1/cs5425/assigment1/WordCount/data/task1-input1.txt");
        Path inputPath2 = args.length >= 1 ? new Path(args[1]) : new Path("/Users/pangyaran/Documents/master-semester1/cs5425/assigment1/WordCount/data/task1-input2.txt");
        Path stopWordPath = args.length >= 1 ? new Path(args[2]) : new Path("/Users/pangyaran/Documents/master-semester1/cs5425/assigment1/WordCount/data/stopwords.txt");
        Path outputPath = args.length >= 1 ? new Path(args[3]) : new Path("/Users/pangyaran/Documents/master-semester1/cs5425/assigment1/WordCount/topkCommonWord/output");
        if (outputPath != null) {
            deleteFolder(new File(outputPath.toString()));
        }

        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        Path tmpPath = new Path(workingDir, "topkCommonWord/tmp");
        fs.delete(tmpPath, true);

        conf.set("stopwords.path", stopWordPath.toString());
        Job jobCount = Job.getInstance(conf, "TopkCountWords");
        jobCount.setJarByClass(TopkCommonWords.class);
        jobCount.setMapperClass(TopkCommonWords.TokenizerMapper.class);
        jobCount.setMapOutputKeyClass(Text.class);
        jobCount.setMapOutputValueClass(MapWritable.class);
        jobCount.setCombinerClass(TopkCommonWords.IntSumReducer.class);
        jobCount.setReducerClass(TopkCommonWords.IntSumReducer.class);
        jobCount.setOutputKeyClass(Text.class);
        jobCount.setOutputValueClass(MapWritable.class);
        FileInputFormat.addInputPath(jobCount, inputPath1);
        FileInputFormat.addInputPath(jobCount, inputPath2);
        FileOutputFormat.setOutputPath(jobCount, tmpPath);
        jobCount.waitForCompletion(true);

        Job jobTopk = Job.getInstance(conf, "jobTopk");
        jobTopk.setJarByClass(TopkCommonWords.class);
        jobTopk.setMapperClass(TopkMapper.class);
        jobTopk.setMapOutputKeyClass(Text.class);
        jobTopk.setMapOutputValueClass(IntWritable.class);
//        jobTopk.setCombinerClass(TopkCommonWords.IntSumReducer.class);
        jobTopk.setReducerClass(TopkCommonWords.TopkReducer.class);
        jobTopk.setOutputKeyClass(Text.class);
        jobTopk.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(jobTopk, tmpPath);
        FileOutputFormat.setOutputPath(jobTopk, outputPath);
//        System.out.println("job:"+job);

        System.exit(jobTopk.waitForCompletion(true) ? 0 : 1);

    }

    public static void deleteFolder(File file) {
        if (file == null || file.listFiles() == null) {
            return;
        }
        for (File subFile : file.listFiles()) {
            if (subFile.isDirectory()) {
                deleteFolder(subFile);
            } else {
                subFile.delete();
            }
        }
        file.delete();
    }

}
